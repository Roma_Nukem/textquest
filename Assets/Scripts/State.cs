﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject
{
    [SerializeField] string LocationName;
    [TextArea(10,25)][SerializeField] string storyText;
    [SerializeField] State[] nextStates;

    public string GetLocationName()
    {
        return LocationName;
    }

    public string GetStoryText()
    {
        return storyText;
    }
        
    public State[] GetStoryState()
    {
        return nextStates;
    }
}
